import Router from 'koa-router';
import summariesController from './controllers/summaries-controller';
import checkUser from '../../handlers/checkUser';
import checkSummary from './handlers/checkSummary';
import { Summary } from './models';
const router = new Router({ prefix: '/summaries' });

router
    .post('/', checkUser(), summariesController.create)
    .param('hash', checkSummary())
    .get('/:hash', checkUser(), summariesController.get)
    .put('/:hash', checkUser(), summariesController.update)
    .delete('/:hash', checkUser(), summariesController.delete);

export {
  Summary,
};

export default router.routes();
