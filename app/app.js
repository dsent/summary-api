import Koa from 'koa';
import connectorInit from './connectors';
import initHandlers from './handlers';
import modules from './modules';
import AppError from './helpers/appError';

connectorInit();
global.AppError = AppError;

const app = new Koa();

initHandlers(app);
app.use(modules);

app.use(async (ctx) => {
  ctx.body = '<h1>Test</h1>';
});

export default app;
